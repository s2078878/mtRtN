//Profile config names for nf-core/configs
params {
  config_profile_description = 'University of Edinburgh (eddie) cluster profile provided by nf-core/configs.'
  config_profile_contact = 'Alison Meynert (@ameynert)'
  config_profile_url = 'https://www.ed.ac.uk/information-services/research-support/research-computing/ecdf/high-performance-computing'

  parameters = false
  purity = false
  outdir = "$baseDir"
  matched = true
  chr = "chrM"
  reference = "$baseDir/data/chrM.fa"
  rnaseq = false
}

executor {
  name = "sge"
  queueSize = "100"
}

process {
  clusterOptions = { task.memory ? "-l h_vmem=${task.memory.bytes/task.cpus}" : null }
  scratch = true
  penv = { task.cpus > 1 ? "sharedmem" : null }

  // common SGE error statuses
  errorStrategy = {task.exitStatus in [143,137,104,134,139,140] ? 'retry' : 'finish'}
  maxErrors = '-1'
  maxRetries = 3

  beforeScript =
  """
  . /etc/profile.d/modules.sh
  module load anaconda
  source activate mtRtN
  export pid=''
  """

  // Process-specific resource requirements
  withName: index {
    clusterOptions = "-l h_vmem=6G -l h_rt=6:00:00"  
  }
  withName: rtn {
    clusterOptions = "-l h_vmem=6G -l h_rt=6:00:00"  
  }
  withName: pileup {
    clusterOptions = "-l h_vmem=6G -l h_rt=6:00:00"  
  }
  withName: varscan {
    clusterOptions = "-l h_vmem=6G -l h_rt=6:00:00"  
  }
}
