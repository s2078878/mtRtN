######################################################
# This script filters the merged SNP and Indel calls #
######################################################

# load command line arguments
args = commandArgs(trailingOnly=T)

# necessary packages
library(data.table)
library(dplyr)

# read in the merged files 
snps <- fread(args[1])
indels <- fread(args[2])
matched <- args[3]

if(matched == "true"){
  
  somaticFilterMatched <- function(varscanOutput){
    
    aocs <- varscanOutput
    aocs$tumor_var_freq <- as.numeric(sub("%", "", aocs$tumor_var_freq))
    aocs$normal_var_freq <- as.numeric(sub("%", "", aocs$normal_var_freq))

    aocs <- filter(aocs, somatic_status == "Somatic")
    snps <- aocs
    
    # low complexity regions
    snps <- filter(snps, position <= 66 | position >= 71)
    snps <- filter(snps, position <= 300 | position >= 316)
    snps <- filter(snps, position <= 513 | position >= 525)
    snps <- filter(snps, position <= 3106 | position >= 3107)
    snps <- filter(snps, position <= 12418 | position >= 12425)
    snps <- filter(snps, position <= 16182 | position >= 16194)
    
    snps <- filter(snps, tumor_reads2 >= 30)
    snps <- filter(snps, tumor_var_freq >= 0.25)
    
    snps_sfrb <- data.frame()
    
    if(dim(snps)[1] != 0){
      
    for(i in 1:nrow(snps)){
        
        row <- snps[i]
        cont <- data.frame(ref = c(row$normal_reads1_plus, row$normal_reads1_minus),
                           alt = c(row$normal_reads2_plus, row$normal_reads2_minus),
                           row.names = c("plus", "minus"))
        
        test <- fisher.test(cont)
        p_value <- test$p.value
        
        row$strand_bias <- p_value
        row$phred_score <- -10 * log10(row$strand_bias)
        
        snps_sfrb <- rbind(snps_sfrb, row)
        
    }
      
      snps_sfrb <- filter(snps_sfrb, phred_score < 60)
    
    } else {
      
      snps_sfrb <- snps
      
    }
    
    snps_bi <- data.frame()
    
    if(dim(snps_sfrb)[1] != 0){
      
      for(i in unique(snps_sfrb$position)){
        
        one_pos <- filter(snps_sfrb, position == i)
        
        if(length(unique(one_pos$var)) == 1){
          
          snps_bi <- rbind(snps_bi, one_pos)
          
        }
      }
      
    } else {
      
      snps_bi <- snps_sfrb
      
    }
    
    snps_bi
    
  }
  
  germlineFilterMatched <- function(varscanOutput){
    
    aocs <- varscanOutput
    aocs$tumor_var_freq <- as.numeric(sub("%", "", aocs$tumor_var_freq))
    aocs$normal_var_freq <- as.numeric(sub("%", "", aocs$normal_var_freq))

    aocs <- filter(aocs, somatic_status == "Germline")
    
    snps <- aocs
    
    # low complexity regions
    snps <- filter(snps, position <= 66 | position >= 71)
    snps <- filter(snps, position <= 300 | position >= 316)
    snps <- filter(snps, position <= 513 | position >= 525)
    snps <- filter(snps, position <= 3106 | position >= 3107)
    snps <- filter(snps, position <= 12418 | position >= 12425)
    snps <- filter(snps, position <= 16182 | position >= 16194)
    
    snps <- filter(snps, normal_reads2 >= 30)
    snps <- filter(snps, normal_var_freq >= 0.25)
    
    snps_sfrb <- data.frame()
    
    if(dim(snps)[1] != 0){
      
      for(i in 1:nrow(snps)){
        
        row <- snps[i]
        cont <- data.frame(ref = c(row$normal_reads1_plus, row$normal_reads1_minus),
                           alt = c(row$normal_reads2_plus, row$normal_reads2_minus),
                           row.names = c("plus", "minus"))
        
        test <- fisher.test(cont)
        p_value <- test$p.value
        
        row$strand_bias <- p_value
        row$phred_score <- -10 * log10(row$strand_bias)
        
        snps_sfrb <- rbind(snps_sfrb, row)
        
      }
      snps_sfrb <- filter(snps_sfrb, phred_score < 60)
    } else {
      
      snps_sfrb <- snps
      
    }
    
    snps_bi <- data.frame()
    
    if(dim(snps_sfrb)[1] != 0){
      
      for(i in unique(snps_sfrb$position)){
        
        one_pos <- filter(snps_sfrb, position == i)
        
        if(length(unique(one_pos$var)) == 1){
          
          snps_bi <- rbind(snps_bi, one_pos)
          
        }
      }
      
    } else {
      
      snps_bi <- snps_sfrb
      
    }
  
    snps_bi
    
  }
  
  if(dim(snps)[1] != 0){
    somatic_snvs <- somaticFilterMatched(snps)
  } else {
    somatic_snvs <- snps
  }
  
  if(dim(indels)[1] != 0){
    somatic_indels <- somaticFilterMatched(indels)
  } else {
    somatic_indels <- indels
  }
  
  if(dim(snps)[1] != 0){
    germline_snvs <- germlineFilterMatched(snps)
  } else {
    germline_snvs <- snps
  }

  if(dim(indels)[1] != 0){
    germline_indels <- germlineFilterMatched(indels)
  } else {
    germline_indels <- indels
  }

  # output the final filtered dataframes
  fwrite(somatic_snvs, "somaticSNVs.txt", sep = "\t", quote = F, row.names = F)
  fwrite(somatic_indels, "somaticIndels.txt", sep = "\t", quote = F, row.names = F)
  
  fwrite(germline_snvs, "germlineSNVs.txt", sep = "\t", quote = F, row.names = F)
  fwrite(germline_indels, "germlineIndels.txt", sep = "\t", quote = F, row.names = F)

} else {
  
  filterUnmatched <- function(varscanOutput){
    
    aocs <- varscanOutput
    
    aocs <- aocs %>% subset(., select = which(!duplicated(names(.))))
    setDT(aocs)[,c('Cons','Cov','Reads1','Reads2','Freq','P-value'):=tstrsplit(`Cons:Cov:Reads1:Reads2:Freq:P-value`,":")]
    setDT(aocs)[,c('StrandFilter','R1+','R1-','R2+','R2-','pval','pval2'):=tstrsplit(`StrandFilter:R1+:R1-:R2+:R2-:pval`,":")]
    colnames(aocs)[1:4] <- c("chrom", "position", "ref", "var")
    
    aocs$position <- as.numeric(aocs$position)
    aocs$Reads1 <- as.numeric(aocs$Reads1)
    aocs$Reads2 <- as.numeric(aocs$Reads2)
    aocs$`R1+` <- as.numeric(aocs$`R1+`)
    aocs$`R1-` <- as.numeric(aocs$`R1-`)
    aocs$`R2+` <- as.numeric(aocs$`R2+`)
    aocs$`R2-` <- as.numeric(aocs$`R2-`)

    snps <- aocs
    
    # low complexity regions
    snps <- filter(snps, position <= 66 | position >= 71)
    snps <- filter(snps, position <= 300 | position >= 316)
    snps <- filter(snps, position <= 513 | position >= 525)
    snps <- filter(snps, position <= 3106 | position >= 3107)
    snps <- filter(snps, position <= 12418 | position >= 12425)
    snps <- filter(snps, position <= 16182 | position >= 16194)
    
    snps <- filter(snps, Reads2 >= 30)
    snps <- filter(snps, Freq >= 0.25)
    
    snps_sfrb <- data.frame()
    
    if(dim(snps)[1] != 0){
      
      for(i in 1:nrow(snps)){
        
        row <- snps[i]
        cont <- data.frame(ref = c(row$`R1+`, row$`R1-`),
                           alt = c(row$`R2+`, row$`R2-`),
                           row.names = c("plus", "minus"))
        
        test <- fisher.test(cont)
        p_value <- test$p.value
        
        row$strand_bias <- p_value
        row$phred_score <- -10 * log10(row$strand_bias)
        
        snps_sfrb <- rbind(snps_sfrb, row)
        
      }
      
      snps_sfrb <- filter(snps_sfrb, phred_score < 60)
      
    } else {
      
      snps_sfrb <- snps
      
    }
    
    snps_bi <- data.frame()
    
    if(dim(snps_sfrb)[1] != 0){
      
      for(i in unique(snps_sfrb$position)){
        
        one_pos <- filter(snps_sfrb, position == i)
        
        if(length(unique(one_pos$var)) == 1){
          
          snps_bi <- rbind(snps_bi, one_pos)
          
        }
      }
      
    } else {
      
      snps_bi <- snps_sfrb
      
    }
    
    snps_bi
    
  }
  
  if(dim(snps)[1] != 0){
    filtered_snvs <- filterUnmatched(snps)
  } else {
    filtered_snvs <- snps
  }
  
  if(dim(indels)[1] != 0){
    filtered_indels <- filterUnmatched(indels)
  } else {
    filtered_indels <- indels
  }
  
  # output the final filtered dataframes
  fwrite(filtered_snvs, "filteredSNVs.txt", sep = "\t", quote = F, row.names = F)
  fwrite(filtered_indels, "filteredIndels.txt", sep = "\t", quote = F, row.names = F)

}












