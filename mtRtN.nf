#!/usr/bin/env nextflow

log.info """\
         =======================================================
         M T R T N :   M T D N A   V A R I A N T   C A L L I N G
         =======================================================
         parameters_file:   ${params.parameters}
         purity_file:       ${params.purity}
         out_directory:     ${params.outdir}
         matched_samples:   ${params.matched}
         chr_name:          ${params.chr}
         rna-seq:           ${params.rnaseq}
         =======================================================
         """
         .stripIndent()

/*
 * Input parameters validation
 */
if (params.parameters){
    
    parameters_file = file(params.parameters)
        
    Channel
        .fromPath(params.parameters)
        .splitCsv(header:true)
        .map{ row -> tuple(row.row, row.sampleID, row.group, row.bam, row.index)}
        .set{ parameters_ch }

    if( !parameters_file.exists() ) exit 1, "ERROR: Parameters file doesn't exist: ${parameters_file}"

} else {
    
    exit 1, "ERROR: Missing parameters file. Please set a parameters file using the --parameters flag."

}

if (params.purity){
    
    purity_file = file(params.purity)

    Channel
        .fromPath(params.purity)
        .splitCsv(header:true)
        .map{ row -> tuple(row.row, row.sampleID, row.group, row.purity) }
        .set{ purity_ch }
    
    if( !purity_file.exists() ) exit 1, "Purity file doesn't exist: ${purity_file}"

} else {
    
    purity_file = "false"

}

/*
 * Create index files for alignment
 */
process index {
    tag "Creating index files"

    output:
    path("RtN/Calabrese_Dayama_Smart_Numts.*") into numt_ch
    path("RtN/humans.*") into humans_ch
    path("RtN/Nix_binary/rtn") into binary_ch

    script:
    if( "${params.rnaseq}" == "false" )
        """
        git clone https://github.com/Ahhgust/RtN.git
        bunzip2 RtN/humans.fa.bz2 && bwa index RtN/humans.fa
        """
    else if( "${params.rnaseq}" == "true" )
        """
        mkdir RtN
        mkdir RtN/Nix_binary
        touch RtN/Calabrese_Dayama_Smart_Numts.txt
        touch RtN/humans.txt
        touch RtN/Nix_binary/rtn
        """
    else
        exit 1, "ERROR: Invalid rnaseq mode: ${params.rnaseq}\nCheck that --rnaseq is set to true or false."
        
}

/*
 * Extract only the mitochondria reads from the bam files
 */
process chrm {
    tag "Pulling chrM from bam file"

    input:
    set row, sampleID, group, path(bam), path(index) from parameters_ch

    output:
    set sampleID, group, path("${sampleID}.${group}.bam") into mitochondria_ch

    script:
    """
    samtools view -h -b ${bam} ${params.chr} > ${sampleID}.${group}.bam
    """
}

/*
 * Run RtN on chrM bam file to remove NUMT reads
 */
process rtn {
    tag "Removing NUMT reads"

    input:
    set sampleID, group, path(chrM_bam) from mitochondria_ch
    path(executable) from binary_ch
    path(reference) from humans_ch
    path(numts) from numt_ch

    output:
    set sampleID, group, path("${sampleID}.${group}.rtn.bam") into filt_ch

    script:
    if( "${params.rnaseq}" == "false" )
        """
        ./rtn -p -h humans.fa -n Calabrese_Dayama_Smart_Numts.fa -b ${chrM_bam}
        """
    else if( "${params.rnaseq}" == "true" )
        """
        mv ${chrM_bam} ${sampleID}.${group}.rtn.bam
        """
    else
        exit 1, "ERROR: Invalid rnaseq mode: ${params.rnaseq}\nCheck that --rnaseq is set to true or false."
}

/*
 * Sort the files and create mpileup file ready for varscan
 */
process pileup {
    tag "Sorting and creating pileup files"

    input:
    set sampleID, group, path(filtered_chrM) from filt_ch

    output:
    set sampleID, group, path("${sampleID}.${group}.pileup") into mpileup_ch

    script:
    """
    samtools sort ${filtered_chrM} | \
    samtools mpileup -d 0 -f $baseDir/data/chrM.fa - > ${sampleID}.${group}.pileup
    """
}

if(purity_file != "false" && params.matched == "true"){

    // Merge the pileup files and the purity data
    mpileup_ch
        .mix(purity_ch)
        .groupTuple(by:[0,1]) 
        .map { input -> tuple(input[0], input[2][0], input[2][1]) }
        .groupTuple(by:0)
        .map { input -> tuple(input[0], input[1][0], input[1][1], input[2][0], input[2][1]) }
        .set { merged_pileup_ch }

} else if(purity_file == "false" && params.matched == "true") {

    mpileup_ch
        .groupTuple(by:0)
        .map { sampleID, groups, files -> tuple( sampleID, groups, files.sort{it.name} ) }
        .map { input -> tuple(input[0], 1, 1, input[2][0], input[2][1]) }
        .set { merged_pileup_ch }

} else {

    mpileup_ch
        .map { input -> tuple(input[0], "EMPTY", "EMPTY", input[2], "$baseDir/data/MT.fa") }
        .set { merged_pileup_ch }

}

/*
 * Run VarScan2 to call mitochondrial DNA variants and if they are germline/somatic
 */
process varscan {
    tag "Running VarScan2"

    input:
    set sampleID, n_purity, t_purity, path(n_pileup), path(t_pileup) from merged_pileup_ch

    output:
    set path("${sampleID}.snp"), path("${sampleID}.indel") into var_ch

    script:
    if( "${params.matched}" == "true" )
        """
        varscan somatic ${n_pileup} ${t_pileup} ${sampleID} \
        --strand-filter 1 --min-avg-qual 30 --min-coverage 2 --min-reads 2 --min-var-freq 0 \
        --normal-purity ${n_purity} --tumor-purity ${t_purity}
        """
    else if( "${params.matched}" == "false" )
        """
        varscan mpileup2snp ${n_pileup} \
            --strand-filter 1 --min-avg-qual 30 \
            --min-coverage 2 --min-reads2 2 --min-var-freq 0 > ${sampleID}.snp

        varscan mpileup2indel ${n_pileup} \
            --strand-filter 1 --min-avg-qual 30 \
            --min-coverage 2 --min-reads2 2 --min-var-freq 0 > ${sampleID}.indel
        """
    else
        exit 1, "ERROR: Invalid matched-sample mode: ${params.matched}\nCheck that --matched is set to true or false."
}

/*
 * Merge all variant calls
 */
process merge {
    tag "Merging variant calls"
    publishDir "${params.outdir}/results/" , mode:'copy'

    input:
    path(var_calls) from var_ch.collect()

    output:
    set path("combinedSNPs.txt"), path("combinedIndels.txt") into combined_ch

    script:
    """
    cp $baseDir/scripts/mergeCalls.R .
    Rscript mergeCalls.R ${var_calls}
    """
}

/*
 * Filter the variant calls
 */
process filter {
    tag "Filtering variant calls"
    publishDir "${params.outdir}/results/" , mode:'copy'

    input:
    set path(snps), path(indels) from combined_ch

    output:
    set path("somaticSNVs.txt"), path("somaticIndels.txt"), path("germlineSNVs.txt"), path("germlineIndels.txt"), path("filteredSNVs.txt"), path("filteredIndels.txt")  optional true into filtered_ch

    script:
    """
    cp $baseDir/scripts/filterCalls.R .
    Rscript filterCalls.R ${snps} ${indels} ${params.matched}
    """
}

workflow.onComplete {
	log.info ( workflow.success ? "\nDone! All mitochondrial DNA variants called.\n" : "\nOops .. something went wrong.\n" )
}
